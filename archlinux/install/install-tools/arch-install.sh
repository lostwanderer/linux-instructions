#!/usr/bin/env zsh
# The purpose of this script is
# 	-make computer bootable
# 	-get to ability to run sysyadm bootstrap
return 2 # dont remove this until ready to run

# input variables 
drive=/dev/sda

esp_part_device=/dev/sda1
esp_part_label=esp

swap_part_device=/dev/sda2
swap_part_label=swap

sys_part=/dev/sda3
sys_part_label=arch-linux

is_ssd=false
swap_size=16GiB # 2x RAM if hibernation is used about (see wiki for more info)
cpu_manufacturer=intel # intell or amd
new_hostname=roemer

# partition the device
sgdisk --zap-all "$drive" #zap the drive of all partition tables
sgdisk --clear \
	--new=1:0:+512MiB	--typecode=1:ef00	--change-name=1:$esp_part_label \
	--new=2:0:+"$swap_size"	--typecode=2:8200	--change-name=2:$swap_part_label \
	--new=3:0:0		--typecode=3:8300	--change-name=3:$sys_part_label \
	"$drive"

# setup second partition (swap partition)
# NOTE the setup of the first(esp) partition needs to be done after the third is mounted
mkswap -L $swap_part_label $swap_part_device
swapon -L $swap_part_label #needed for genfstab

# setup third partition (system partition)
mkfs.btrfs -f -L $sys_part_label $sys_part_device
# mount entire btrfs partition
mount -L $sys_part_label /mnt
# create subvolumes
btrfs sub create /mnt/@
btrfs sub create /mnt/@home
btrfs sub create /mnt/@snapshots
btrfs sub create /mnt/@pkg
btrfs sub create /mnt/@var-log
btrfs sub create /mnt/@var-tmp
umount -R /mnt

# mount btrfs subvolumes
# quickly process whether to use the ssd option
if "$is_ssd"; then
	ssd_option=',ssd'
fi
o="compress=lzo,lazytime,relatime$ssd_option"
mount -o $o,subvol=@ -L $sys_part_label /mnt
mkdir -p /mnt/home
mkdir -p /mnt/.snapshots
mkdir -p /mnt/var/cache/pacman/pkg
mkdir -p /mnt/var/log
mkdir -p /mnt/var/tmp
mount -o $o,subvol=@home -L "$sys_part_label" /mnt/home
mount -o $o,subvol=@snapshots -L "$sys_part_label" /mnt/.snapshots
mount -o $o,subvol=@pkg -L "$sys_part_label" /mnt/var/cache/pacman/pkg
mount -o $o,subvol=@var-log -L "$sys_part_label" /mnt/var/log
mount -o $o,subvol=@var-tmp -L "$sys_part_label" /mnt/var/tmp

# setup first partition (efi system partition[esp])
# format the drive
mkfs.fat -F32 -n $esp_part_label $esp_part_device
# mount efi sys partition
mkdir /mnt/boot
mount -L $esp_part_label /mnt/boot

# install packages
pacstrap /mnt linux linux-firmware base btrfs-progs
# generate fstab file
genfstab -L /mnt >> /mnt/etc/fstab

# copy mkinitcpio.conf 
cp ./mkinitcpio-minimal.conf /etc/mkinitcpio.conf

# create the config files for systemd-boot
boot_entries_dir='/mnt/boot/loader/entries'
mkdir -p "$boot_entries_dir"
fill_in_template () {
	sed "
	s|TITLE|$1|
	s|IS_FALLBACK|$2|
	s|CPU_MANUFACTURER|$3|
	s|SYS_PART_LABEL|$4|
	" $5
}
arch_args=(
	'Arch Linux'
	''
	"$cpu_manufacturer"
	"$sys_part_label"
	'./bootmenu-item-template.conf'
) 
arch_fallback_args=(
	'Arch Linux (Fallback)'
	'-fallback'
	"$cpu_manufacturer"
	"$sys_part_label"
	'./bootmenu-item-template.conf'
) 
fill_in_template "${arch_args[@]}" > "$boot_entries_dir/arch-linux.conf"
fill_in_template "${arch_fallback_args[@]}" > "$boot_entries_dir/arch-linux-fallback.conf"


# chroot into the system 
cmd_str="
	ln -sf /usr/share/zoneinfo/America/New\ York /etc/localtime
	hwclock --systohc

	sed -i 's|^#en_US.UTF-8 UTF-8|en_US.UTF-8 UTF-8|' /etc/locale.gen
	locale-gen
	echo 'LANG=en_US.UTF-8' >> /etc/locale.conf
	
	echo '127.0.0.1	localhost' >> /etc/hosts
	echo '::1		localhost' >> /etc/hosts
	echo '127.0.1.1	$new_hostname.localdomain	$new_hostname' >> /etc/hosts

	pacman -S --needed $cpu_manufacturer-ucode base-devel networkmanager openssh yadm git

	mkinitcpio -P
	bootctl install

	systemctl enable NetworkManager
	sed -i 's|^#PermitRootLogin prohibit-password|PermitRootLogin yes|' /etc/ssh/sshd_config

	passwd
	"
arch-chroot /mnt bash -c "$cmd_str"
