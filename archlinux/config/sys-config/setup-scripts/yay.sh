# Make the yay package 
printf '\n\nDownloading and Installing yay\n'

if [ ! $(which yay) ]; then
	# Require git to be installed
	pacman -S --needed git
	# Remember original directory
	initialwd="$(pwd)"
	# Create build directory with admin user permissions
	build_dir="/home/$admin_user/build-yay"
	sudo -u $admin_user mkdir "$build_dir"
	# Clone the repo as admin
	sudo -u $admin_user git clone https://aur.archlinux.org/yay.git "$build_dir"
	# Enter dirctory
	cd "$build_dir"
	# Build and install package with dependancies as admin
	sudo -u "$admin_user" makepkg -si
	# Return to original directory
	cd "$initialwd"
	# Remove build directory
	sudo -u "$admin_user" rm -rf $build_dir
fi
