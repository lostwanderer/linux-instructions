# Posix shell code for starting and enabling reflector
printf '\n\nSetting up Reflector for Mirrors\n'

# Install and enable reflector
pacman -S --needed reflector
systemctl enable --now reflector.timer
# Run the service once
printf '\tUpdate mirrors now [y/N]? '
read yn
: "${yn:=N}"
[ "$yn" == 'y' ] && systemctl start reflector.service
unset yn
