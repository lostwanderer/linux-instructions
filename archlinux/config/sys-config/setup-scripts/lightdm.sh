# Posix shell script for setting the lightdm display manager
printf '\n\nSetup Display Manager (lightdm)\n'

# Install display manager 
pacman -S --needed xorg 
pacman -S --needed lightdm 
pacman -S --needed lightdm-gtk-greeter
# Install theming
pacman -S --needed pop-gtk-theme
# Install cursors
pacman -S --needed xcursor-simpleandsoft
# Install icons (hicolor is needed for about all other icons)
pacman -S --needed hicolor-icon-theme 
pacman -S --needed papirus-icon-theme
# Install font
pacman -S --needed otf-fira-mono

# Enable display manager
systemctl enable lightdm.service
