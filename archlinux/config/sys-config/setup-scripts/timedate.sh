# Posix shell code for setting up clocks
printf '\n\nSetting Software and Hardware Clock\n'

# Enable New_York timezone
timedatectl set-timezone "$(prompt_with_def 'Enter timezone' 'America/New_York')"
# Enable autoupdates of the software clock
timedatectl set-ntp true 
# Set the hardware clock
hwclock --systohc
