# Posix shell code for an interactive adding of user
printf '\n\nCreating admin user\n'

# Add user and set their password if they exist
useradd -m "$admin_user" && passwd $admin_user
# Add admin user to appropriate groups
usermod -aG "adm,games,log,rfkill,sys,systemd-journal,wheel" "$admin_user"

