# Setup locale settings
printf '\n\nSetup Hostname\n'

# Set hostname from the system_name var that bootstrap sets initially
hostnamectl hostname "$system_name"
