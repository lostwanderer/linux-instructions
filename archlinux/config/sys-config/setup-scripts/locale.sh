# Setup locale settings
printf '\n\nSetup Locale\n'

# Ask to install cli packages
localectl set-locale LANG="$(prompt_with_def 'Enter a locale' 'en_US.UTF-8')"

# Setup locale (need to add stuff from install)
localectl set-keymap --no-convert "$(prompt_with_def 'Enter a keymap' 'us')"
