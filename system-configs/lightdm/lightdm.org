#+TITLE: Lightdm
* install lightdm and greeter
** sudo pacman -S lightdm lightdm-gtk-greeter
* enable the service
** systemctl enable lightdm.service
* nice gui for customization
** sudo pacman -S lightdm-gtk-greeter-settings
